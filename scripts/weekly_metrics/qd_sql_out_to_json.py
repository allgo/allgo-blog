#! /usr/bin/env python3

## AIMS  : reformat SQL response in json.
## USAGE : cat sql.out | ./qd_sql_out_to_json.py

"""
Input is like
users
26

##
apps	private
15	0
16	1

##
jobs	result
3	0
139	1
106	2
1	3
4	4

"""

import sys
import json

# ========================================
KEYS = {
    'private': ['public', 'private'],
    'result' : ['NONE', 'SUCCESS', 'ERROR', 'ABORTED', 'TIMEOUT']
}

# defines only the output keys
output = {
    'users': None,
    'apps' : None,
    'jobs' : None
}

# ----------------------------------------
class Dataset:
    def __init__(self, main_key):
        self.__key = main_key

    @property
    def key(self):
        return self.__key

    @property
    def values(self):
        raise NotImplementedError

    def store(self, out_dict):
        out_dict[self.key] = self.values

class OneValue(Dataset):
    def __init__(self, main_key):
        super().__init__(main_key)
        self.__value = None

    def new_value(self, elts):
        self.__value = elts[0]

    def get_dict(self):
        return {self.key: self.__value}

    @property
    def values(self):
        return self.__value

class GroupValues(Dataset):
    def __init__(self, elts):
        super().__init__(elts[0])
        self.__keys   = KEYS[elts[1]]
        self.__values = dict()
        for k in self.__keys:
            self.__values[k] = 0

    def new_value(self, elts):
        key = self.__keys[int(elts[1])]
        self.__values[key] = elts[0]

    def get_dict(self):
        return {self.key: self.__values}

    @property
    def values(self):
        return self.__values

# ========================================
if __name__ == '__main__':
    # ~ print( "INIT\n" + str(output) )

    o_ds  = None
    lines = [line.rstrip("\n") for line in sys.stdin]
    for line in lines:
        if '' is line:
            continue

        elts = line.split("\t")
        if '##' == elts[0]:
            if o_ds is not None:
                # a whole query result has been parsed
                # o_ds contains this dataset
                # it is turned into a dict
                # which is used to update the output
                output.update(o_ds.get_dict())
                o_ds = None

        elif o_ds is None:
            # we start a new dataset, type depending on the number of column
            if 1 == len(elts):
                o_ds = OneValue(elts[0])
            else:
                o_ds = GroupValues(elts)

        else:
            # a new record to add in the dataset
            o_ds.new_value(elts)

    if o_ds is not None:
        # manage the last dataset
        output.update(o_ds.get_dict())

    # turn the dictionnary output
    # into json string
    print(json.dumps(output))

    sys.exit(0)
