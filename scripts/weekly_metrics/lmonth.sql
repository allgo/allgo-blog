-- copy of lweek.sql with the @borne_inf updated to 1 month

set @borne_inf = date_sub( now(), interval 1 month );

-- ~ # nb user
SELECT count(*) AS users from auth_user
    WHERE @borne_inf <= date_joined;

SELECT "##" as '';
-- ~ # nb applis
SELECT count(*) as apps, private FROM dj_webapps 
    WHERE @borne_inf <= created_at
    GROUP BY private;

SELECT "##" as '';
-- ~ # nb jobs
SELECT count(*) as jobs, result FROM dj_jobs
    WHERE @borne_inf <= created_at
    GROUP BY result;
