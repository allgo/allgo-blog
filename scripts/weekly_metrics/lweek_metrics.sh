#! /bin/bash

## AIMS  : update the report file, with last week (from today) metrics.
## USAGE : ./lweek_metrics.sh
## NOTE  : even if it produces json, it does not update existing object
## NOTE  :  which mean, *do not* launch twice the same day.

set -o nounset
set -o errexit

# =========================================================

# ---------------------------------------------------------
declare -r PROG_PATH=$( dirname $0 | xargs realpath )

declare -r SERVER="woody"
declare -r INSTANCE="prod18"
declare -r DB="allgo"

declare -r REPORT_FILE="$PROG_PATH/report.json" # init it with [] if doesn't exist.
declare -r REPORT_FILE_PREV="$PROG_PATH/report_prev.json"

# everything under TMP_DIR will be erased.
declare -r TMP_DIR=$( mktemp -d )
declare -r BLOG_PATH="$TMP_DIR/AllGo/dépot/Blog"
declare -r BLOG_CSV_PATH="$BLOG_PATH/content/AllGo/metrics"
declare -r BLOG_ARTICLE_PATH="$BLOG_PATH/content/AllGo/metrics"

declare -r BLOG_URL="git@gitlab.inria.fr:allgo/allgo-blog.git"
declare -r BLOG_BRANCH="lweek_metrics"
declare -r BLOG_MASTER="master"

declare -r LOG_PREFIX="#LOG\t"

trap before_exiting EXIT

# =========================================================
function before_exiting ()
{
    printf "TRAP : $0 exit.\n" >&2

    rm -rf "$TMP_DIR"
    # erasing can be a pb during debug
    #   but it is needed not to polute the machine
}

function printVars ()
{
    printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
    printf "╟ - SCRIPT     = $0\n"
    printf "╟ - LOG_PREFIX = $LOG_PREFIX\n"
    printf "╟ - PROG_PATH  = $PROG_PATH\n"
    printf "╟\n"
    printf "╟ - SERVER     = $SERVER\n"
    printf "╟ - INSTANCE   = $INSTANCE\n"
    printf "╟ - DB         = $DB\n"
    printf "╟\n"
    printf "╟ - REPORT_FILE       = $REPORT_FILE\n"
    printf "╟ - REPORT_FILE_PREV  = $REPORT_FILE_PREV\n"
    printf "╟\n"
    printf "╟ - BLOG_PATH         = $BLOG_PATH\n"
    printf "╟ - BLOG_CSV_PATH     = $BLOG_CSV_PATH\n"
    printf "╟ - BLOG_ARTICLE_PATH = $BLOG_ARTICLE_PATH\n"
    printf "╟\n"
    printf "╟ - BLOG_URL    = $BLOG_URL\n"
    printf "╟ - BLOG_BRANCH = $BLOG_BRANCH\n"
    printf "╟ - BLOG_MASTER = $BLOG_MASTER\n"
    printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function get_sql_queries_as_json ()
{
    local query_file="$1"

    ssh "$SERVER" dk-mysql $INSTANCE $DB \
        < "$query_file" \
        | "$PROG_PATH/qd_sql_out_to_json.py"
}

function update_report ()
{
    printf "${LOG_PREFIX}Updating the report $REPORT_FILE ..."

    if [ ! -f "$REPORT_FILE" ]
    then
        printf "[]\n" > "$REPORT_FILE"
    fi

    local jq_filter='. += [{
        date: "'$( date +%Y-%m-%d )'",
        global: '$( get_sql_queries_as_json "$PROG_PATH/global.sql" )',
        "last week": '$( get_sql_queries_as_json "$PROG_PATH/lweek.sql" )',
        "last month": '$( get_sql_queries_as_json "$PROG_PATH/lmonth.sql" )',
        "last year": '$( get_sql_queries_as_json "$PROG_PATH/lyear.sql" )'
    }]'

    local new_file=$( mktemp -t allgo.XXXX.json )

    jq "$jq_filter" "$REPORT_FILE" \
        > "$new_file"

    mv "$REPORT_FILE" "$REPORT_FILE_PREV"
    mv "$new_file" "$REPORT_FILE"

    printf "\tdone.\n"
}

# =========================================================
function prepare_blog ()
{
    printf "${LOG_PREFIX}Get a local copy of the blog ..."
    mkdir -p "$BLOG_PATH"

    git clone "$BLOG_URL" "$BLOG_PATH"
    cd "$BLOG_PATH"
    git checkout -b "$BLOG_BRANCH"

    printf "\tdone.\n"
}

function generate_blog_article ()
{
    printf "${LOG_PREFIX}Generate the article ..."

    local -r str_date=$( date +%Y_%m_%d )
    local -r csv_filename="$str_date.csv"

    mkdir -p "$BLOG_CSV_PATH"

    # generate csv file
    jq '.[-1]' "$REPORT_FILE" \
            | "$PROG_PATH/qd_record_to_csv.py" \
        > "$BLOG_CSV_PATH/$csv_filename"

    mkdir -p "$BLOG_ARTICLE_PATH"

    "$PROG_PATH/generate_metric_blog_article.py" \
        --outfile "$BLOG_ARTICLE_PATH/$str_date.rst" \
        "$csv_filename"

    printf "\tdone -> $BLOG_ARTICLE_PATH/$str_date.rst.\n"
}

function publish_blog ()
{
    printf "${LOG_PREFIX}Last, publishing the article (=push the article on the master branch) ..."

    {
        git add "$BLOG_CSV_PATH" "$BLOG_ARTICLE_PATH"
        git commit -am "automatic commit."
        git checkout "$BLOG_MASTER"
        git merge "$BLOG_BRANCH" -m "automatic merge - New weekly stats article."
        git branch -d "$BLOG_BRANCH"
        git push origin "$BLOG_MASTER"
    } >&2 # redirect to stderr. Only my log are on stdout.

    printf "\tdone.\n"

    cd "$PROG_PATH"
}

# =========================================================
function main ()
{
    update_report
    prepare_blog
    generate_blog_article
    publish_blog
}

# =========================================================
printVars >&2

# your work.
main

printf "End of %s.\n\n" $( basename $0 ) >&2

exit 0
