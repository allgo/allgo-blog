# weekly_metrics

## Aims

update (or create) the report file (report.json), with last week (from today) metrics.  

## Usage

To run it, go in the `weekly_metrics` directory and run
```sh
./lweek_metrics.sh
```

## details

### *.sql

Those files contain sql commands executed by `lweek_metrics.sh` on the database with
```sh
ssh "$SERVER" dk-mysql $INSTANCE $DB \
        < "$sql_file"
```
The `SELECT "##" as '';` is used as a separator for reformat by `qd_sql_out_to_json.py`.

- global.sql : get counts on the whole database.
- lweek.sql : get counts on the last 7 days.
- lmonth.sql : get counts on the last month. [FIXME: what if we are the 31 of the month (not august), like october, there is no 31 september]
- lyear.sql : get counts on the last year.

### qd_sql_out_to_json.py
reformat SQL response in json.

### qd_record_to_csv.py
turn the last json record into a csv table.

### generate_metric_blog_article.py
generate a rst file to display the csv content as table.  
/!\ the slug (~id) for this article varies only by the week number, which means that if it is launched twice on the same week, it will generate a second article with the same slug, and this will leads to problem when generating html content.

The article content is only the csv content as a table.

### lweek_metrics.sh
This is the main script.  
It first updates the report file = run the sql queries + reformat them as json string.  
Then it clones the blog repository on the machine and set a new branch.  
It generates the csv file and the article.  
Last it adds the files to the repository and merge the branch into the master branch.

