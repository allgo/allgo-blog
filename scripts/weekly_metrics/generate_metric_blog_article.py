#!/usr/bin/env python3

## AIMS  : Generate a blog (pelican/rst) article to display csv file content.
## USAGE : generate_metric_blog_article.py [--outfile outfile.rst] filename.csv
## NOTE  : the file content is not read by this script


import sys
import argparse # option management, close to C++/boost library (?)
import time


VERSION = "0.1"

# ========================================
def printMeta( o_fs ):
    o_fs.write("Weekly metric report\n")
    o_fs.write("####################\n\n")

    # %V = ISO 8601 week as a decimal number with Monday as the first day of the week.
    #   Week 01 is the week containing Jan 4. Examples: 01, 02, …, 53
    summary = 'Metrics on week {}'.format(time.strftime("%V"))

    meta = {
        'date': time.strftime("%Y-%m-%d"),
        'category': 'metrics',
        # ~ 'tags': '?',
        'slug': summary.replace(' ', '-'),
        'lang': 'eng',
        'author': 'scripts',

        'summary': summary,
    }
    for k,v in meta.items():
        o_fs.write(":{}: {}\n".format(k, v))

    o_fs.write("\n")

    return

def printArticle( o_fs, csv_file ):
    o_fs.write("Weekly generated metrics\n")
    o_fs.write("========================\n\n")
    o_fs.write(".. csv-table:: Last week metrics.\n")
    o_fs.write("   :file: {}\n".format(csv_file))
    o_fs.write("   :header-rows: 1\n")



def main( o_args ):
    printMeta( o_args.outfile )
    printArticle( o_args.outfile, o_args.csv_filename )

    return

# ----------------------------------------
def defineOptions():
    descr = "Generate a blog (pelican/rst) article to display csv file content."
    o_parser = argparse.ArgumentParser( description=descr )

    # special
    o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

    # positionnal / mandatory param
    o_parser.add_argument( 'csv_filename', type=str,
                       help='filename of the metric content (should be accessible by the blog server).' )

    # optionnal
    o_parser.add_argument( '--outfile', '-o',
                            type=argparse.FileType( 'w' ),
                            default=sys.stdout,
                            help='outfile, default is stdout.' )

    return o_parser

# ----------------------------------------

if __name__ == '__main__':
    o_parser = defineOptions()
    o_args   = o_parser.parse_args()

    main( o_args )

    sys.exit( 0 )

