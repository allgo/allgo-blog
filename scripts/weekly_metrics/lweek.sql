-- ~ # Which strategy to compute the date diff ?

-- ~ SET profiling = 1;
-- ~ SELECT count(*) FROM dj_jobs WHERE YEAR(NOW()) = YEAR(created_at) && month(created_at) = month(now())-1;
-- ~ SELECT created_at, DateDiff(now(), created_at) FROM dj_jobs WHERE DateDiff(now(), created_at) <= 30;
-- ~ set @borne_inf = date_sub( now(), interval 30 day );
-- ~ select created_at, @borne_inf from dj_jobs WHERE created_at >= @borne_inf;

-- ~ SHOW PROFILES;
-- ~ +----------+------------+---------------------------------------------------------------------------------------------------------+
-- ~ | Query_ID | Duration   | Query                                                                                                   |
-- ~ +----------+------------+---------------------------------------------------------------------------------------------------------+
-- ~ |        1 | 0.00442859 | SELECT count(*) FROM dj_jobs WHERE YEAR(NOW()) = YEAR(created_at) && month(created_at) = month(now())-1 |
-- ~ |        2 | 0.00049273 | SELECT created_at, DateDiff(now(), created_at) FROM dj_jobs WHERE DateDiff(now(), created_at) <= 30     |
-- ~ |        3 | 0.00013616 | set @borne_inf = date_sub( now(), interval 30 day )                                                     |
-- ~ |        4 | 0.00045639 | select created_at, @borne_inf from dj_jobs WHERE created_at >= @borne_inf                               |
-- ~ +----------+------------+---------------------------------------------------------------------------------------------------------+
-- ~ # => IMO, the query 4 is easier to read than the 2.
-- ~ #   So i'll use a variable.

set @borne_inf = date_sub( now(), interval 7 day );

-- ~ # nb user
SELECT count(*) AS users from auth_user
    WHERE @borne_inf <= date_joined;

SELECT "##" as '';
-- ~ # nb applis
SELECT count(*) as apps, private FROM dj_webapps 
    WHERE @borne_inf <= created_at
    GROUP BY private;

SELECT "##" as '';
-- ~ # nb jobs
SELECT count(*) as jobs, result FROM dj_jobs
    WHERE @borne_inf <= created_at
    GROUP BY result;
