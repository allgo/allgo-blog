-- ~ # nb user
SELECT count(*) AS users from auth_user;

SELECT "##" as '';
-- ~ # nb applis
SELECT count(*) as apps, private FROM dj_webapps 
    GROUP BY private;

SELECT "##" as '';
-- ~ # nb jobs
SELECT count(*) as jobs, result FROM dj_jobs
    GROUP BY result;
