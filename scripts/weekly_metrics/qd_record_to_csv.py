#! /usr/bin/env python3
"""
The input json looks like
{
  "date": "2019-07-23",
  "global": {
    "users": "26",
    "apps": {
      "public": "15",
      "private": "16"
    },
    "jobs": {
      "NONE": "3",
      "SUCCESS": "139",
      "ERROR": "106",
      "ABORTED": "1",
      "TIMEOUT": "4"
    }
  },
  "last week": {
    "users": "0",
    "apps": null,
    "jobs": {
      "NONE": 0,
      "SUCCESS": "7",
      "ERROR": "1",
      "ABORTED": 0,
      "TIMEOUT": 0
    }
  },
  "last month": { ... },
  "last year": { ... }
}
"""

import json, sys;
import csv
from collections import defaultdict

d_report = json.load(sys.stdin);

headers = sorted( d_report.keys() );


""" defines keys/subkeys

    parse all headers except 'date', the first one
    to generate this dict
{
  'users': set(),
  'apps': {'public', 'private'},
  'jobs': {'ABORTED', 'TIMEOUT', 'SUCCESS', 'ERROR', 'NONE'}
}
"""
d_headers = {}
for key in ( headers[1:] ): # g,w
    for k,d in d_report[key].items(): #u,a,j
        if k not in d_headers:
            d_headers[k] = []

        if isinstance(d,dict):
            d_headers[k] += d.keys()
for k in d_headers: # remove doublons
    d_headers[k] = set(d_headers[k])
# print(d_headers)


""" build table header

    generate 2 csv lines like

date,users,apps,,jobs,,,,
2019-07-23,,private,public,ABORTED,ERROR,NONE,SUCCESS,TIMEOUT
"""

headers_1 = [ "date", ]
headers_2 = [ d_report["date"] ]
for k,s in d_headers.items():
    # ~ print("*{} -> {} - {}".format(k, s, type(s)))
    headers_1.append(k)
    if 0 == len(s):
        headers_2.append(None)

    headers_1 += [None for _ in range(len(s)-1)]
    headers_2 += [ _   for _ in sorted(s)]

#print(headers_1)
#print(headers_2)

""" generate the data rows
    2 rows are build as list :
['global', '26', '16', '15', '1', '106', '3', '139', '4']
['last week', '0', 'NA', 'NA', 0, '1', 0, '7', 0]
"""
matrix = [ headers_1, headers_2 ]
#for key in ( headers[2:] ): # g,w
for key in ( "last week", "last month", "last year" ): # I want this order
    #print(d_report[key])
    row = [key,]

    for k1 in headers_1[1:]:
        if None is k1:
            continue
        v = d_report[key][k1]
        if isinstance(v,dict):
            for k2 in sorted(v):
                row.append(v[k2])
        elif None is v:
            row += ['NA' for _ in d_headers[k1]]
        else:
            row.append(v)
        #print( "{} : {}".format(k1, v))

    matrix.append(row)

o_writer = csv.writer(sys.stdout)
o_writer.writerows(matrix)
