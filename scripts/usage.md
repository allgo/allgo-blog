Scripts
=======

weekly_metrics
--------------

update (or create) the report file (report.json), with last week (from today) metrics.  
To run it, go in the `weekly_metrics` directory and run
```sh
./lweek_metrics.sh
```

### Notes
- do not run the script twice a week (on different day)  
  It will produce a new record, but the article id is based on the week number.  
  So 2 articles will be produced with the same id, and the html generation will complain.

- do not run the script twice a day.  
  It will produce doublon in the report.  
  No big deal, you can edit the report file to manually remove it.

### misc
- get more information in `weekly_metrics/readme.md`
