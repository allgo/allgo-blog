A||GO Blog
================

Aims
----

This is the repository of the blog for the A||Go project.  
It aims to give informations of the project activity.

Install locally
---------------

Clone the repository, then
```sh
make serve [PORT=8001] &
make html
```

Last check the result on localhost.

Badges
------

[![pipeline status](https://gitlab.inria.fr/allgo/allgo-blog/badges/master/pipeline.svg)](https://gitlab.inria.fr/allgo/allgo-blog/commits/master)
