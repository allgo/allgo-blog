A||Go presentation at Cargo conference
######################################

:date: 2019-10-11
:category: comm
:tags: event, prez, vidz
:slug: cargo-2019-presentation
:lang: en
:author: allgo_team
:summary: media elements (=prez) of the Cargo day 2019

A||go has been presented at the Cargo Day meeting in 2019
=========================================================

| Remember, that was on the 14th of june 2019, in Nantes, the Cargo Day.  
| `previous article <{filename}./allgo_at_cargo_2019.rst>`_
|
| A||go has been presented to the audience_.  
| Today we (finally ^_^') offer you links to the media.
|
| Here it the pdf_ <- in french !
| And the video_ (mkv, h264, no sound, 8.8MO) that shows how to add an application.

.. _audience: https://indico.mathrice.fr/event/169/session/0/contribution/5
.. _pdf: /media/communications/Cargo.2019_06.V1_1.pdf
.. _video: /media/communications/new_app.ssr.cargo_demo.mkv
