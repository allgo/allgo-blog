PACAP supports Allgo !
######################

:date: 2019-11-22
:category: news
:tags: financial-support,
:slug: pacap-supports-allgo
:lang: en
:author: allgo_team
:summary: PACAP team funds 4 mounths of development.

PACAP team supports Allgo development
=====================================

The `PACAP team`_ does not yet have the utility of Allgo, 
despite this the team believes in the project and is funding 4 months of development.

.. image:: media/images/2019_11.pacap_support.jpg
           :alt: Patricia Bournai, Erwen Rohou and André Seznec, official gold-support card delivery
           :width: 80%

This allows `Sébastien Letort`_ to work on the project until february 2020.

As a counterpart `Erven Rohou`_ (PACAP team leader) receive the "Gold support card" from `Patricia Bournai`_ (SED team manager) 
in the presence of `André Seznec`_ (owner of the Intel contract used to fund Allgo)
which give its owner a happy welcome in dev. office, team training with personnal support,
and a member place in the user board.

If you think Allgo is a good project, don't hesitate to contact the team on allgo@inria.fr to discuss.  
We need support (fund, dev time or pull request) to achieve all our goals.


.. _`PACAP team`: https://team.inria.fr/pacap/
.. _Erven Rohou: https://team.inria.fr/pacap/members/rohou/
.. _André Seznec: https://team.inria.fr/pacap/members/andre-seznec/
.. _Patricia Bournai: https://fr.linkedin.com/in/patricia-bournai-55477289
.. _Sébastien Letort: https://www.researchgate.net/profile/Sebastien_Letort
