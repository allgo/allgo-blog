Allgo at the Cargo Day !
##########################

:date: 2019-05-29
:category: comm
:tags: prez
:slug: allgo-at-cargo-2019
:lang: en
:author: allgo_team
:summary: We plan to make a presentation at the Cargo meeting.

Cargo meeting comes soon
========================

On the 14th of june 2019 comes the CargoDay_ X "When Dev Meets Ops".

We will be there and plan to present Allgo_.
It will be 15 min. long, and maybe include a tiny demo.

.. _CargoDay: https://indico.mathrice.fr/event/169/overview
.. _AllGo: https://allgo18.inria.fr
