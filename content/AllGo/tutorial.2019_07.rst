A||Go tutorial in Rennes
########################

:date: 2019-07-01
:category: news
:tags: event
:slug: allgo-tutorial-rennes
:lang: en
:author: allgo_team
:summary: Tutorial session on 4th of July

Learn how to use A||Go, in our lab
==================================

On this 4th of July, we will perform a tutorial session, at Irisa, Beaulieu campus.

In this session you will learn how to use A||Go to install an application,
to launch jobs through the web site and using the API,
and even how to launch jobs in a Jupyter notebook.

Please register to https://bit.ly/allgo19
