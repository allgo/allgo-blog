A||Go presentation at SED plenary
#################################

:date: 2019-11-27
:category: comm
:tags: event, prez
:slug: cargo-2019-plenary
:lang: en
:author: allgo_team
:summary: Allgo has been presented at the SED plenary session.
:status: draft

A||go as a computing resource
=============================

Allgo has been presented as a computing plateform.
Despite the fact that its first aim is to present demonstrators, Allgo can be used as a computing plateform.
Indeed, if one provide enough computational resources (Runner), everything is ok to use Allgo intensively.

Furthermore, one of our milestones for 2020 is to enhance the hability for a user to provide its own Runner when submitting a job.

A||go as it is, a way to deploy demo
====================================

As said, the first aim of Allgo is to provide a showroom for scientist applications.
And this is how Allgo has been presented in another session.

It seems that most of people have already heard of Allgo.  
We probably now have to solicit scientists from other centres.

Here are the slides_ presented during the meeting.

.. _slides: media/communications/Plenary.2019_11.pdf
