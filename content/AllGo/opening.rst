A blog for A||go project !
##########################

:date: 2019-05-23
:category: news
:tags: blog
:slug: opening
:lang: en
:author: allgo_team
:summary: First article of the blog.

Here comes the blog :)
======================

This blog will talk about the AllGo_ project.

We will report the activity around the project.

.. _AllGo: https://allgo18.inria.fr
