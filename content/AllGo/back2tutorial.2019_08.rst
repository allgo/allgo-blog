Back to A||Go tutorial in Rennes
################################

:date: 2019-08-01
:category: news
:tags: tuto
:slug: back-to-tutorial-2019-08
:lang: en
:author: allgo_team
:summary: Back to tutorial session of the 4th of July

There was a tutorial session in Rennes
======================================

On the 4th July 2019, in our lab at Rennes-Irisa, there was a tutorial session for A||Go.

A little less than 10 people attended the training.
They learned how to install a new application in A||Go,
how to launch jobs through the web interface, how to use the API
and discovered how to use A||Go in notebooks.

The tutorial came in the form of a notebook, and guess what ?
You can try it at home :)

Download the notebook here_.
If you don't have access to a jupyter server, you can view it on the nbviewer_, but you'll lose some features.

.. _here: https://gitlab.inria.fr/sed-rennes/formations/allgo/tree/master
.. _nbviewer: https://nbviewer.jupyter.org/urls/gitlab.inria.fr/sed-rennes/formations/allgo/raw/master/Formation%20Allgo.ipynb
