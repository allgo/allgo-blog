Allgo goes public :)
##########################

:date: 2019-06-07
:category: news
:tags: code
:slug: allgo-goes-public
:lang: en
:author: allgo_team
:summary: Allgo is available with Affero GPL.

From us to everyone
===================

This week, we placed A||Go under the `Affero GPL`_.

This means that everyone is able to install A||Go on his own computers,  
that everyone can help us to add feature, submit bug correction.

This can be seen as the birth of the A||Go community ;)

-> go check the code_

.. _Affero GPL: https://www.gnu.org/licenses/agpl-3.0.en.html
.. _code: https://gitlab.inria.fr/allgo/allgo
