About A||Go
###########

:date: 2019-05-20
:tags: allgo,blog
:slug: about
:authors: allgo_team
:summary: short description of A||go project

A||gorithms at your fingertips

A||go is a platform for building and deploying apps that analyze massive data.

It has been specifically designed for science applications.
